#!/usr/bin/env python

import db
import gui
import pygame

class Fralph:
    def __init__(self):
        self._db = db.Db('db')
        self._gui = gui.Gui(self)

    def __call__(self, gui, event):
        if event.key == pygame.K_ESCAPE:
            return False
        group = self._db.get_rand_by_letter(event.unicode)
        if not group:
            return True
        self._gui.clear()
        if group['image']:
            self._gui.output_image(group['image'])
        if group['sound']:
            self._gui.play_sound(group['sound'])
        self._gui.output_text(group['text'])
        return True

    def run(self):
        self._gui.run()

fralph = Fralph()
fralph.run()
