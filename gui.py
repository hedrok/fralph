#!/usr/bin/env python

import pygame

class Gui:
    def __init__(self, on_key):
        self._on_key = on_key

    def run(self):
        self._screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
        self._aspect_ratio = self._get_aspect_ratio(self._screen)
        pygame.display.init()
        pygame.font.init()
        pygame.mixer.init()
        pygame.mixer.set_num_channels(1)

        self._font = pygame.font.Font('NotoSerif-Regular.ttf', 128)

        while True:
            event = pygame.event.wait()
            if event.type == pygame.QUIT:
                break
            if event.type == pygame.KEYDOWN:
                if not self._on_key(self, event):
                    break
            pygame.display.flip()
        pygame.quit()

    def clear(self):
        self._screen.fill((0, 0, 0))

    def output_image(self, filename):
        img = pygame.image.load(filename).convert()
        ar = self._get_aspect_ratio(img)
        if img.get_width() <= self._screen.get_width() and img.get_height() <= self._screen.get_height():
            sz = img.get_size()
            pt = (int((self._screen.get_width() - w) / 2.), int((self._screen.get_height() - h) / 2.))
        if ar > self._aspect_ratio:
            h = int(img.get_height() * self._screen.get_width() / img.get_width())
            sz = (self._screen.get_width(), h)
            pt = (0, int((self._screen.get_height() - h) / 2.))
        else:
            w = int(img.get_width() * (self._screen.get_height() / img.get_height()))
            sz = (w, self._screen.get_height())
            pt = (int((self._screen.get_width() - w) / 2.), 0)
        img = pygame.transform.scale(img, sz)
        self._screen.blit(img, pt)

    def play_sound(self, filename):
        pygame.mixer.stop()
        snd = pygame.mixer.Sound(filename)
        snd.play()

    def output_text(self, text):
        surf = self._font.render(text, True, (0, 0, 0), (255, 255, 255))
        self._screen.blit(surf, (self._screen.get_width() - surf.get_width(), 0))

    @staticmethod
    def _get_aspect_ratio(surface):
        sz = surface.get_size()
        return sz[0] / sz[1]

    
if __name__ == '__main__':
    def on_key(gui, event):
        return event.key != pygame.K_ESCAPE

    g = Gui(on_key)
    g.run()
