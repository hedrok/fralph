#!/usr/bin/env python
import collections
import os
import random

debug = False

class Db:
    """
    Database

    Current design: one folder with a lot of files
    Files that have same name prefix before last .[a-z] are one group.
    All files with .jpg/.png/.gif are considered image of group.
    All files with .ogg/.mp3 are considered sound of group
    .txt file in first line is text that will be displayed (other lines are ignored)
    First letter of text is key in database

    Currently several image files or sound files is an error.
    """

    def __init__(self, path):
        """
        Initialsizes database

        @param path Path to folder with database
        """
        lst = os.listdir(path)
        if debug:
            print(f'Reading path "{path}" returned "{lst}"')
        groups = collections.defaultdict(dict)
        extensions = {
            '.jpg': 'image',
            '.png': 'image',
            '.gif': 'image',
            '.wav': 'sound',
            '.ogg': 'sound',
            '.mp3': 'sound',
            '.txt': 'textfile',
        }
        for f in lst:
            name, ext = os.path.splitext(f)
            if ext not in extensions:
                if debug:
                    print(f"Debug: ignoring file '{f}', unknown extension '{ext}'")
                continue
            key = extensions[ext]
            if key in groups[name]:
                raise ValueError(f'Two files in same group: "{name}" for type "{key}". First: "{groups[name][key]}", second: "{f}"')
            groups[name][key] = os.path.join(path, f)
            if debug:
                print(f"Debug: added key '{key}' to group '{name}'")
        self._db = collections.defaultdict(list)
        for groupname, group in groups.items():
            if 'textfile' not in group:
                print(f"Info: group {groupname} has no .txt file and will be ignored.")
                continue
            with open(group['textfile']) as f:
                text = f.readline().strip()
            letter = text[0].lower()
            group['text'] = text
            for k in 'image', 'sound':
                if k not in group:
                    print(f"Info: group {groupname} missed {k} field.")
                    group[k] = None
            self._db[letter].append(group)


    def get_rand_by_letter(self, letter):
        """
        Returns random item of get_all_by_letter(letter)
        """
        lst = self.get_all_by_letter(letter)
        if not lst:
            return None
        return random.choice(lst)


    def get_all_by_letter(self, letter):
        """
        Gets all groups by letter

        Returns list of dicts:
        {
            'image': filename,
            'sound': filename,
            'textfile': filename,
            'text': string,
        }

        Both 'image' and 'sound' may be None if no file for group is found

        List is softed by 'text' of dicts. If 'text' is equal, sorted by 'image', then 'sound'
        """
        return self._db.get(letter, [])

if __name__ == '__main__':
    debug = True
    test = Db('db')
    print(test._db)
